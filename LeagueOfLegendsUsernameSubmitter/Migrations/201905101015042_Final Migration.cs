namespace LeagueOfLegendsUsernameSubmitter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FinalMigration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Player", "Comment_commentId", "dbo.Comment");
            DropIndex("dbo.Player", new[] { "Comment_commentId" });
            AddColumn("dbo.Comment", "Playerdetails_id", c => c.Int());
            CreateIndex("dbo.Comment", "Playerdetails_id");
            AddForeignKey("dbo.Comment", "Playerdetails_id", "dbo.Player", "id");
            DropColumn("dbo.Player", "Comment_commentId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Player", "Comment_commentId", c => c.Int());
            DropForeignKey("dbo.Comment", "Playerdetails_id", "dbo.Player");
            DropIndex("dbo.Comment", new[] { "Playerdetails_id" });
            DropColumn("dbo.Comment", "Playerdetails_id");
            CreateIndex("dbo.Player", "Comment_commentId");
            AddForeignKey("dbo.Player", "Comment_commentId", "dbo.Comment", "commentId");
        }
    }
}
