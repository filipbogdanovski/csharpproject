namespace LeagueOfLegendsUsernameSubmitter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class heyyy : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Player", "ImagePath");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Player", "ImagePath", c => c.String());
        }
    }
}
