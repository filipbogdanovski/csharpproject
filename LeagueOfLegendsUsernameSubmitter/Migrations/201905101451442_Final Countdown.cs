namespace LeagueOfLegendsUsernameSubmitter.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FinalCountdown : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comment", "Playerdetails_id", "dbo.Player");
            DropIndex("dbo.Comment", new[] { "Playerdetails_id" });
            AddColumn("dbo.Comment", "Description", c => c.String());
            AddColumn("dbo.Comment", "id", c => c.Int(nullable: false));
            DropColumn("dbo.Comment", "Playerdetails_id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Comment", "Playerdetails_id", c => c.Int());
            DropColumn("dbo.Comment", "id");
            DropColumn("dbo.Comment", "Description");
            CreateIndex("dbo.Comment", "Playerdetails_id");
            AddForeignKey("dbo.Comment", "Playerdetails_id", "dbo.Player", "id");
        }
    }
}
