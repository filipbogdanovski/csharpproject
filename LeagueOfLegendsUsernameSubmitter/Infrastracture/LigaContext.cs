﻿using LeagueOfLegendsUsernameSubmitter.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace LeagueOfLegendsUsernameSubmitter.Infrastracture
{
    public class LigaContext : DbContext
    {



    public LigaContext() : base("LigaContext")
        {

        }      


    public DbSet<Player> Players { get; set; } 
    public DbSet<Comment> Comments { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }




    }
}