﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LeagueOfLegendsUsernameSubmitter.Models
{
    public class Player
    {
        [Required]     
        public string SummonerName { get; set; }
        public int id { get; set; }
        public string rank { get; set; }
        
        public string Description { get; set; }
        public DateTime dayposted { get; set; }
        public string region { get; set; }

       

    }
}