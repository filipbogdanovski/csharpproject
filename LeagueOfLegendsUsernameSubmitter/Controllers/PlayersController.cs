﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using LeagueOfLegendsUsernameSubmitter.Infrastracture;
using LeagueOfLegendsUsernameSubmitter.Models;
using PagedList;
using PusherServer;
using System.Threading.Tasks;

namespace LeagueOfLegendsUsernameSubmitter.Controllers
{
    public class PlayersController : Controller
    {
        private LigaContext db = new LigaContext();

        // GET: Players
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.NameSortParm = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.DateSortParm = sortOrder == "Date" ? "date_desc" : "Date";


            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;


            var players = from p in db.Players
                           select p;
            if (!String.IsNullOrEmpty(searchString))
            {
                players = players.Where(p => p.SummonerName.Contains(searchString));
                                       
            }
            switch (sortOrder)
            {
                    case "date_desc":
                    players = players.OrderByDescending(p => p.dayposted);
                    break;
                default:
                    players = players.OrderBy(p => p.dayposted);
                    break;
            }
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(players.ToPagedList(pageNumber, pageSize));
        }

        // GET: Players/Details/5
        public ActionResult Details(int? id)
        {               
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Player player = db.Players.Find(id);
            if (player == null)
            {
                return HttpNotFound();
            }
          


            return View(player);
        }

        // GET: Players/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Players/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,SummonerName,rank,Description,dayposted")] Player player)
        {
            if (ModelState.IsValid)
            {
                player.dayposted = DateTime.Now;
                db.Players.Add(player);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(player);
        }

        // GET: Players/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Player player = db.Players.Find(id);
            if (player == null)
            {
                return HttpNotFound();
            }
            return View(player);
        }

        // POST: Players/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,SummonerName,rank,profilePhoto,Description,dayposted")] Player player)
        {
            if (ModelState.IsValid)
            {
                player.dayposted = DateTime.Now;
                db.Entry(player).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(player);
        }

        // GET: Players/Delete/5
        public ActionResult Comments(int? id)
        {
            var comments = db.Comments.Where(x => x.id == id).ToArray();
            return Json(comments, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> Comment(Comment data)
        {
            db.Comments.Add(data);
            db.SaveChanges();
            var options = new PusherOptions();
            options.Cluster = "eu";
            var pusher = new Pusher("779904", "f690692a8ba64ca95b50", "d2c9c224e9f6f5efe3fd", options);
            ITriggerResult result = await pusher.TriggerAsync("asp_channel", "asp_event", data);
            return Content("ok");
        }



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
